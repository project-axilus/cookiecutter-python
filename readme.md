[Cookiecutter Python][github_pages]
===================================
[![GitHub version][github_badge]][github_tags] [![Build status][travis_badge]][travis] 

_Providing a cookiecutter template for python projects._

License
-------
Copyright © Mr Axilus.
This project is licensed under [CC BY-NC-SA 4.0][license].

[github_badge]: https://img.shields.io/github/release/mraxilus/cookiecutter-python.svg
[github_pages]: http://mr.axilus.name/cookiecutter-python/
[github_tags]: https://github.com/mraxilus/cookiecutter-python/tags
[license]: https://creativecommons.org/licenses/by-nc-sa/4.0/
[travis]: https://secure.travis-ci.org/mraxilus/cookiecutter-python
[travis_badge]: https://img.shields.io/travis/mraxilus/cookiecutter-python.svg


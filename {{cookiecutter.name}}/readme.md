[{{cookiecutter.name}}][github_pages]
===============================
[![GitHub version][github_badge]][github_tags] [![Build status][travis_badge]][travis] [![Code coverage][codecov_badge]][codecov]

_{{cookiecutter.description}}_

License
-------
Copyright © {{cookiecutter.author}}.
This project is licensed under [{{cookiecutter.license_name}}][license].

[codecov]: https://codecov.io/github/{{cookiecutter.username}}/{{cookiecutter.name}}
[codecov_badge]: https://img.shields.io/codecov/c/github/{{cookiecutter.username}}/{{cookiecutter.name}}.svg
[github_badge]: https://img.shields.io/github/release/{{cookiecutter.username}}/{{cookiecutter.name}}.svg
[github_pages]: {{cookiecutter.url}}
[github_tags]: https://github.com/{{cookiecutter.username}}/{{cookiecutter.name}}/tags
[license]: {{cookiecutter.license_url}}
[travis]: https://secure.travis-ci.org/{{cookiecutter.username}}/{{cookiecutter.name}}
[travis_badge]: https://img.shields.io/travis/{{cookiecutter.username}}/{{cookiecutter.name}}.svg

